var bcx = require('../lib/basecamp')
  , moment = require('moment')
  , empty = require('lodash.isempty')
  , omit = require('lodash.omit')
  , map = require('lodash.map')
  , find = require('lodash.find')
  , transform = require('lodash.transform')
  , assign = require('lodash.assign')
  , async = require('async')
  , csv = require('csv-stringify')
  , parallel = require('async').parallel
  , models = require('../lib/app').models;


setInterval(function () {
  var timeout = Math.floor(Date.now()/1000) - 60;
  var sql = 'UPDATE "Punch" SET "finished"=true WHERE "end" <= ' + timeout;
  models.punch.query(sql, function (err, results) {
    if (err) throw err;
  });
}, 10000);

exports.getPunch = function (req, res, next) {
  var uid = Number(req.query.user_id || req.session.data.identity.id);
  var hasntSharedAccess = !find(req.session.data.shareAccesses, { user_id: uid });
  var isNotCurrentUser = req.session.data.identity.id !== uid;
  if (hasntSharedAccess && isNotCurrentUser)
    return res.sendStatus(403);

  var query = {
    user_id: uid,
    start: {
      '>=': moment(req.query.start || null).startOf('day').unix(),
      '<=': moment(req.query.end || null).endOf('day').unix()
    }
  };

  if (req.query.project_id)
    query.project_id = req.query.project_id;

  models.punch
    .find(query)
    .sort({ createdAt: 'desc' })
    .exec(function (err, punches) {
      if (err) return next(err);
      return res.json(punches);
    });
};

exports.postPunch = function (req, res, next) {

  if ((empty(req.body.todo) && empty(req.body.message)) && empty(req.body.project))
    return res.sendStatus(500);

  var obj = {
    project_id: req.body.project.id || null,
    account_id: req.body.account.id || null,
    user_id: req.session.data.identity.id,
    message_id: req.body.message.id || null,
    todo_id: req.body.todo.id || null,
    start: Math.floor(Date.now() / 1000),
    end: Math.floor(Date.now() / 1000),
    finished: false,
    edited: false
  };

  assign(req.body, obj);

  models.punch.create(obj).exec(function (err, punch) {
    if (err) return next(err);
    return res.json(assign(punch, req.body));
  })
};

exports.putPunchId = function (req, res, next) {

  models.punch.findOne({ 
    id: req.params.id, 
    user_id: req.session.data.identity.id
  }).exec(function (err, punch) {
    if (err) return next(err);
    if (!punch) return res.sendStatus(404);

    if (req.body.edited) {
      punch.start = Math.floor(new Date(req.body.start).getTime()/1000);
      punch.end = Math.floor(new Date(req.body.end).getTime()/1000);
      punch.edited = true;
    } else
      punch.end = Math.floor(Date.now()/1000);

    if (!punch.finished)
      punch.finished = req.body.finished || false;

    punch.comment = req.body.comment || false;
    punch.save(function (err, punch) {
      if (err) return next(err);
      return res.json(assign(req.body, omit(punch.toJSON(), 'account','project','message','todo')));
    });
  });

};

