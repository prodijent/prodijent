var filter = require('lodash.filter')
  , map = require('lodash.map')
  , pick = require('lodash.pick')
  , find = require('lodash.find')
  , contains = require('lodash.contains')
  , assign = require('lodash.assign')
  , each = require('lodash.foreach')
  , startsWith = require('lodash.startswith')
  , async = require('async')
  , models = require('../lib/app').models
  , config = require('../lib/config')
  , bcx = require('../lib/basecamp');

exports.getUserMe = bcx.loginUrl;
exports.getUserLoginCb = bcx.authCb;

exports.getUserLogout = function getUserLogout(req, res, next) {
  req.session.destroy(function (err) {
    if (err) return next(err);
    return res.sendStatus(302);
  });
};


exports.getUser = function getUser(req, res, next) {
  bcx.get('people.json', req.session.user, function (err, resp, people) {
    if (err) return next(err);

    if (!req.query.email_address)
      return res.json(people);

    return res.json(filter(people, function (person) {
      return startsWith(person.email_address, req.query.email_address);
    }));
  });
};