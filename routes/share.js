var models = require('../lib/app').models
  , map = require('lodash.map');

exports.getShare = function (req, res, next) {
  models.share.find({ user_id: req.session.data.identity.id })
    .exec(function (err, shares) {
      if (err) return next(err);
      return res.json(shares);
    });
};

exports.postShare = function (req, res, next) {
  models.share.create({
    user_id: req.session.data.identity.id,
    share_id: req.body.share_id
  }).exec(function (err, share) {
    if (err) return next(err);
    return res.json(share);
  })
};

exports.deleteShareId = function (req, res, next) {
  models.share.destroy(req.params.id).exec(function (err) {
    if (err) return next(err);
    return res.sendStatus(204);
  })
};
