#!/usr/bin/env bash
rm -Rf public/dist

mkdir public/tmp
mkdir public/dist
mkdir public/tmp/lib

cd public/tmp


cp ../../client/index.html ./
cp ../../client/app.html ./
cp ../../client/config.js ./
cp ../../client/index.html ./
cp ../../client/index.js ./
cp ../../client/icon.icns ./
cp ../../client/icon.ico ./
cp ../../client/package.json ./
cp -r ../../client/lib/afk ./lib
touch ./.id

/Users/brianberlin/.nvm/versions/node/v0.12.3/bin/nwbuild --macIcns=./icon.icns --winIco=./icon.ico -p win32,win64,osx32,osx64,linux32,linux64 -f ./ -o ../dist

cd ../dist/Prodijent

tar -czf linux32.tar.gz linux32
rm -Rf linux32

tar -czf linux64.tar.gz linux64
rm -Rf linux64

ln -s /Applications osx32/Applications
hdiutil create -volname Prodijent -srcfolder ./osx32 -ov -format UDZO prodijent-32bit.dmg
rm -Rf osx32

ln -s /Applications osx64/Applications
hdiutil create -volname Prodijent -srcfolder ./osx64 -ov -format UDZO prodijent-64bit.dmg
rm -Rf osx64

zip -r win32.zip win32
rm -Rf win32

zip -r win64.zip win64
rm -Rf win64

mv ./* ../
cd ../../../
rmdir ./public/dist/Prodijent

TIME=`date +%s`; webpack; cp client/app.build.js public/app.build.$TIME.js

rm -Rf public/tmp

