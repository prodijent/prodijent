var webpack = require('webpack')
  , autoPrefixer = require('autoprefixer-core')
  , ExtractTextPlugin = require('extract-text-webpack-plugin');


var loaders = [
  { test: /\.(html|md)/, loader: 'html' },
  { test: /\.json$/, loader: 'json' },
  { test: /\.css$/, loader: 'style-loader!css-loader!postcss-loader' },
  { test: /\.js$/, exclude: /node_modules/, loader: "babel-loader"},
  { test: /\.(otf|eot|svg|ttf|woff)/, loader: 'url-loader?limit=10000' },
];

var conf = {
  entry: ['./client/app.js'],
  output: { path: './client/', filename: 'app.build.js' },
  resolve: { modulesDirectories: ['node_modules'] },
  postcss: [autoPrefixer()],
  module: { loaders: loaders },
  target: 'node-webkit',
  plugins: [
    new webpack.optimize.DedupePlugin(),
    new ExtractTextPlugin('app.css', { allChunks: true }),
    new webpack.SourceMapDevToolPlugin('app.build.map.js', null, "[absolute-resource-path]", "[absolute-resource-path]")
  ]
};

if (!process.env.isDev)
  conf.plugins.push(new webpack.optimize.UglifyJsPlugin({
    compress: { warnings: false },
    output: { comments: false },
    sourceMap: false
  }));

module.exports = conf;