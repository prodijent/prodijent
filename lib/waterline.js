var Waterline = require('waterline')
  , postgresAdapter = require('sails-postgresql')
  , orm = new Waterline()
  , fs = require('fs')
  , empty = require('lodash.isempty')
  , config = require('./config')
  , Model = require('./model')
  , events = require('./events')
  , resources = require('../Resources.json');


var ormConfig = {
  adapters: {
    default: config.databases_default + 'Adapter',
    postgres: postgresAdapter
  },
  connections: {
    postgres: {
      url: config.databases_postgres_url,
      adapter: 'postgres'
    }
  },
  defaults: {
    migrate: 'safe'
  }
};

resources.forEach(function eachResource(resource) {
  if (!resource.model) return;
  var modelPath = '../models/' + resource.name + '.js';
  var modelTemplate = fs.readFileSync(__dirname + '/templates/model.js', { encoding: 'utf8' });
  if (!fs.existsSync(__dirname + '/' + modelPath))
    fs.writeFileSync(__dirname + '/' + modelPath, modelTemplate);
  var overrides = require(modelPath);
  var model = new Model(resource, overrides);
  orm.loadCollection(model.Model);
});

orm.initialize(ormConfig, function(err, obj) {
  if (err) throw err;
  events.emit('orm-initialized', obj);
});

