var fs = require('fs')
  , contains = require('lodash.contains')
  , colors = require('colors')
  , prettyjson = require('prettyjson')
  , config = require('./config');

module.exports = function logger(req, res, next) {

  // Skip in production
  if (!config.isDev || !config.debug)
    return next();

  // Skip is just sending client html
  if (req.url === '/')
    return next();

  // Skip if requesting favicon
  if (req.url.indexOf('favicon') !== -1)
    return next();

  // Log
  console.log('\n-------REQUEST-------\n' + colors.blue(req.method), req.url);

  // If post, put or patch log body
  if (contains(['POST', 'PUT', 'PATCH'], req.method))
    console.log(prettyjson.render(req.body));

  // RESPONSE
  var oldWrite = res.write,
    oldEnd = res.end;

  var chunks = [];

  res.write = function (chunk) {
    chunks.push(chunk);
    oldWrite.apply(res, arguments);
  };

  res.end = function (chunk) {
    if (chunk)
      chunks.push(chunk);

    var body = Buffer.concat(chunks).toString('utf8');

    if (body.charAt(0) === '[' || body.charAt(0) === '{')
      try{
        console.log(colors.yellow('\n-------RESPONSE-------\n' + req.method), req.url + '\n', '\r' + prettyjson.render(JSON.parse(body)));
      } catch (e) {}


    oldEnd.apply(res, arguments);
  };

  next();
};