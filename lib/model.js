var Waterline = require('waterline')
  , getSlug = require('speakingurl')
  , uuid = require('uuid').v4
  , isFunction = require('lodash.isfunction')
  , extend = require('lodash.assign')
  , where = require('lodash.findwhere')
  , clone = require('lodash.clone')
  , omit = require('lodash.omit')
  , config = require('./config')

module.exports = Model

function Model(resource, overrides) {
  if (!resource.model) return;
  var model = this;

  var opts = {}

  opts.tableName = resource.name.charAt(0).toUpperCase() + resource.name.slice(1);
  model.type = opts.tableName.toLowerCase();
  opts.connection = config.databases_default;

  var attributes = clone(resource.model.attributes, true);
  for (var x in attributes) {
    delete attributes[x].remove;
    delete attributes[x].inputElement;
    delete attributes[x].inputElementAttributes;
  }

  opts.attributes = extend(attributes, overrides.instanceMethods || {});

  opts.attributes.toJSON = function toJSON() {
    var obj = this.toObject();
    var attributes = resource.model.attributes;
    for (var x in attributes)
      if (attributes[x].remove) delete obj[x];
    return obj;
  };

  overrides.hooks = overrides.hooks || {}

  extend(opts, overrides.classMethods || {});
  extend(opts, overrides.hooks || {});

  model.Model = Waterline.Collection.extend(opts);

}