var config = require('./config');

if (config.twilio_sid && config.twilio_token)
  var client = require('twilio')(config.twilio_sid, config.twilio_token);

module.exports = client || null;