var request = require('request')
  , redis = require('./redis')
  , querystring = require('querystring')
  , extend = require('lodash.assign')
  , pick = require('lodash.pick')
  , difference = require('lodash.difference')
  , contains = require('lodash.contains')
  , pluck = require('lodash.pluck')
  , isObject = require('lodash.isobject')
  , isArray = require('lodash.isarray')
  , filter = require('lodash.filter')
  , empty = require('lodash.isempty')
  , map = require('lodash.map')
  , config = require('./config')
  , models = require('./app').models
  , base = 'https://launchpad.37signals.com/';

var params = {
  client_id: config.bcx_id,
  redirect_uri: config.bcx_cb,
  type: 'web_server'
};

exports.loginUrl = function (req, res) {
  return res.json(extend(req.session.data || {}, { id: req.session.id, href: base + 'authorization/new' + '?' + querystring.stringify(params) }));
};


exports.authCb = function (req, res, next) {
  if (req.query.error)
    return res.redirect('app://app/app.html');

  request({
    method: 'POST',
    url: base + 'authorization/token',
    json: true,
    body: extend({ client_secret: config.bcx_secret, code: req.query.code }, params)
  }, function(err, resp, body) {
    if (err) return next(err);
    var access_token = body.access_token;
    request({
      method: 'GET',
      url: base + 'authorization.json',
      json: true,
      headers: {
        Authorization: 'Bearer ' + access_token
      }
    }, function (err, resp, body) {
      if (err) return next(err);
      if (body.error) return next(body.error);
      models.share.find({ share_id: body.identity.id })
        .exec(function (err, shares) {
          if (err) return next(err);
          req.session.save(extend(body, { access_token: access_token, shareAccesses: shares }));
          return res.redirect('app://app/app.html?id=' + req.session.id);
        });
    });
  });
};

exports.get = function (path, user, cb) {
  if (!user)
    return cb(new Error('no user'));
  if (!user.accounts.length)
    return cb(new Error('no account'));
  var url = user.accounts +'/' + path;
  var redisKey = 'bcx:' + user.id + ':' + path;
  redis.hgetall(redisKey, function (err, results) {
    if (err) return cb(err);
    var headers = {
      'Authorization': 'Bearer ' + user.access_token,
      'User-Agent': 'Prodijent (https://www.prodijent.com)'
    };
    if (results) {
      headers['If-None-Match'] = results.etag || '';
      headers['If-Modified-Since'] = results['last-modified'] || '';
    }
    request({ method: 'GET', url: url, json: true, headers: headers }, function (err, resp, body) {
      if (resp.headers.etag)
        redis.hset(redisKey, 'etag', resp.headers.etag);
      if (resp.headers['last-modified'])
        redis.hset(redisKey, 'last-modified', resp.headers['last-modified']);
      if (body)
        redis.hset(redisKey, 'body', JSON.stringify(body));
      if (resp.statusCode === 304)
        try { body = JSON.parse(results.body); } catch (e) { return cb(e); }
      return cb(err, body);
    });
  });

};
