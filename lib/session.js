var redis = require('./redis')
  , uuid = require('uuid').v4;

module.exports = function (req, res, next) {
  req.session = {};

  req.session.id = req.headers.id || uuid();
  req.session.data = null;

  req.session.save = function (data) {
    req.session.data = data;
    redis.set('sess:' + req.session.id, JSON.stringify(data));
  };

  req.session.destroy = function (cb) {
    redis.del('sess:' + req.session.id, cb);
  };

  if (!req.headers.id) return next();

  redis.get('sess:' + req.session.id, function (err, data) {
    req.session.data = JSON.parse(data);
    next()
  });

};
