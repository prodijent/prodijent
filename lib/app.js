var express = require('express')
  , fs = require('fs')
  , bodyParser = require('body-parser')
  , config = require('./config')
  , waterline = require('./waterline')
  , session = require('./session')
  , events = require('./events')
  , token = require('./token')
  , errorHandler = require('./error-handler')
  , logger = require('./req-res-logger')
  , app = express();


events.on('orm-initialized', function(waterline) {

  app.set('environment', config.isDev ? 'development' : 'production');
  app.disable('x-powered-by');
  app.set('view engine', 'jade');
  app.use(express.static('public'));
  app.use(bodyParser.urlencoded({extended: true}));
  app.use(bodyParser.json(null));
  app.use(session);
  app.use(logger);
  app.models = waterline.collections;

  app.get('/app.build.js', function (req, res) {
    return res.redirect('https://s3.amazonaws.com/prodijent/app.build.' + config.build + '.js')
  });

  require('./resource-routes');

  app.use(errorHandler);

  app.listen(process.env.PORT || config.port, function appListen() {
    console.log('Server started on port:', process.env.PORT || config.port);
    require('./webpack');
  });
});

module.exports = app;