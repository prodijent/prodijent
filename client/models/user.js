import Model from 'ampersand-model'
import config from '../config';

export default Model.extend({
  modelType: 'User',
  urlRoot: config.host + '/api/v1/user',
  props: {
    id: 'number',
    expires_at: 'date',
    access_token: 'string'
  },
  collections: {
    accounts: require('./account-collection'),
    shares: require('./share-collection')
  },
  children: {
    identity: require('./identity')
  },
  derived: {
    fullName: {
      deps: ['identity.first_name','identity.last_name'],
      fn () {
        return [this.identity.first_name, this.identity.last_name].join(' ');
      }
    },
    name: {
      deps: ['identity.first_name','identity.last_name'],
      fn () {
        return [this.identity.first_name, this.identity.last_name].join(' ');
      }
    }
  }
});
