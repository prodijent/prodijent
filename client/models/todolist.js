import Model from 'ampersand-model';

export default Model.extend({
  modelType: 'todolist',
  props: {
    id: 'number',
    completed: 'boolean',
    completed_count: 'number',
    description: 'string',
    name: 'string',
    position: 'number',
    private: 'boolean',
    remaining_count: 'number',
    trashed: 'boolean',
    created_at: 'date',
    url: 'string',
    app_url: 'string',
    updated_at: 'date'
  },
  children: {
    creator: require('./person'),
    bucket: require('./bucket')
  }
});