import Model from 'ampersand-model';

export default Model.extend({
  modelType: 'todolist',
  props: {
    id: 'number',
    identity_id: 'number',
    name: 'string',
    email_address: 'string',
    admin: 'boolean',
    is_client: 'boolean',
    trashed: 'boolean',
    avatar_url: 'string',
    fullsize_avatar_url: 'string',
    created_at: 'date',
    updated_at: 'date',
    //url: 'string',
    //app_url: 'string'
  }
});