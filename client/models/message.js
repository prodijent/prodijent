import app from 'ampersand-app'
import cacheMixin from 'ampersand-local-cache-mixin'
import Model from 'ampersand-model'
import ms from 'milliseconds'
import config from '../config';

module.exports = Model.extend(cacheMixin, {
  modelType: 'message',
  tts: ms.minutes(5),
  ttl: ms.weeks(1),
  storageKey () { return 'message:' + this.id; },
  props: {
    id: 'number',
    subject: 'string',
    created_at: 'date',
    updated_at: 'date',
    content: 'string',
    private: 'boolean',
    trashed: 'boolean'
  },
  children: {
    creator: require('./person')
  },
  collections: {
    comments: require('./comment-collection'),
    subscribers: require('./person-collection')
  },
  initialize () {
    this.initStorage();
    this.on('change', this.writeToStorage, this);
  },
  url () {
    return this.parent.messageApiUrl;
  },
  ajaxConfig () {
    return {
      headers: {
        'Authorization': app.me.access_token ? 'Bearer ' + app.me.access_token : null,
        'User-Agent': 'Prodijent (https://www.prodijent.com)'
      }
    }
  }
});
