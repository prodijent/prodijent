import app from 'ampersand-app'
import Model from 'ampersand-model'
import isNumber from 'lodash.isnumber'
import each from 'lodash.foreach'
import moment from 'moment'
import config from '../config'
import timeDisplay from '../lib/time-display'
import Account from './account'
import User from './user'
import Project from './project'
import Message from './message'
import Todo from './todo'
import slack from '../lib/slack'

export default Model.extend({
  modelType: 'Punch',
  urlRoot: config.host + '/api/v1/punch',
  props: {
    id: 'number',
    comment: 'string',
    start: 'date',
    end: 'date',
    finished: 'boolean',
    edited: 'boolean',
    account_id: 'number',
    user_id: 'number',
    project_id: 'number',
    message_id: 'number',
    todo_id: 'number'
  },
  session: {
    over: 'boolean',
    currentTime: 'date'
  },
  children: {
    account: Account,
    user: User,
    project: Project,
    message: Message,
    todo: Todo
  },
  ajaxConfig () { return { headers: { 'id': app.me.id } } },
  initialize () {
    var model = this;
    var ct = model.currentTime;
    setInterval(function () {
      ct = new Date();
    }, 1000);
    each(['account','project','message','todo'], function (item) {
      model[item].on('change:id', function (itemModel) {
        model[item + '_id'] = itemModel.id;
      });
    });
    model.once('sync', slack);
    model.on('change:finished', () => {
      if (model.finished)
        slack(model);
    });

  },
  parse (attrs) {
    if (attrs.start)
      attrs.start = attrs.start * 1000;

    if (attrs.end)
      attrs.end = attrs.end * 1000;

    return attrs;
  },
  derived: {
    projectApiUrl: {
      deps: ['account_id', 'project_id'],
      cache: false,
      fn () {
        if (!this.account_id || !this.project_id) return '';
        return 'https://basecamp.com/' + this.account_id + '/api/v1/projects/' + this.project_id + '.json';
      }
    },
    messageApiUrl: {
      deps: ['account_id', 'project_id', 'message_id'],
      cache: false,
      fn () {
        if (!this.account_id || !this.message_id) return '';
        return 'https://basecamp.com/' + this.account_id + '/api/v1/projects/' + this.project_id + '/messages/' + this.message_id + '.json';
      }
    },
    todoApiUrl: {
      deps: ['account_id', 'project_id', 'todo_id'],
      cache: false,
      fn () {
        if (!this.account_id || !this.todo_id) return '';
        return 'https://basecamp.com/' + this.account_id + '/api/v1/projects/' + this.project_id + '/todos/' + this.todo_id + '.json';
      }
    },
    label: {
      deps: ['project.name', 'message.subject', 'todo.content'],
      fn () {
        var project = this.project.name || '';
        if (project.length > 50)
          return '<span>' + project.slice(0,50).trim() + '...</span>';
        var end = '';
        if (this.message.id)
          end = end + this.message.subject;
        if (this.todo.id)
          end = end + this.todo.content;
        if (project.length + end.length > 50)
          end = end.slice(0,50 - project.length).trim() + '...';
        return '<span>' + project + '</span>' + end;
      }
    },
    csvLabel: {
      deps: ['project.name', 'message.subject', 'todo.content'],
      fn () {
        var project = this.project.name || '';
        var end = '';
        if (this.message.id)
          end = end + this.message.subject;
        if (this.todo.id)
          end = end + this.todo.content;
        return project.replace(/[^\w\s]/gi, '') + ' - ' + end.replace(/[^\w\s]/gi, '');
      }
    },
    startElementValue: {
      deps: ['start'],
      fn () {
        return moment(this.start).format('HH:mm');
      }
    },
    endElementValue: {
      deps: ['end'],
      fn () {
        return moment(this.end).format('HH:mm');
      }
    },
    startDisplay: {
      deps: ['start'],
      fn () {
        return moment(this.start).format('hh:mm A');
      }
    },
    endDisplay: {
      deps: ['end'],
      fn () {
        return moment(this.end).format('hh:mm A');
      }
    },
    day: {
      deps: ['start'],
      fn () {
        return moment(this.start).format('YYYY-MM-DD');
      }
    },
    dayClass: {
      deps: ['start'],
      fn () {
        return 'day-' + moment(this.start).format('DD');
      }
    },
    milliseconds: {
      deps: ['start','end','finished','started','currentTime'],
      cache: false,
      fn () {
        if (!this.start) return 0;
        return this.finished ? moment(this.end).diff(this.start) : moment(this.currentTime).diff(this.start);
      }
    },
    total: {
      deps: ['milliseconds'],
      cache: false,
      fn () {
        return timeDisplay(this.milliseconds);
      }
    },
    totalHours: {
      deps: ['milliseconds'],
      cache: false,
      fn () {
        var seconds = this.milliseconds / 1000;
        var minutes = seconds / 60;
        var hours = minutes / 60;
        return hours.toFixed(2);
      }
    },
    chartStyles: {
      deps: ['start','end'],
      fn () {
        var bod = new Date(this.start).setHours(0,0,0);
        var ms = new Date(this.start).setHours(23,59,59) - bod;
        var start = this.start.getTime();
        var end = this.end.getTime();
        var width = ((end - start) / ms) * 100;
        var offset = ((start - bod) / ms) * 100;
        return 'width: ' + width + '%; left: ' + offset + '%';
      }
    },
    link: {
      deps: ['id', 'account_id', 'project_id', 'message_id', 'todo_id'],
      fn () {
        if (!this.project || !this.account) return;
        var base = 'https://basecamp.com/' + this.account_id + '/projects/' + this.project_id + '/';
        if (this.message_id) return base + 'messages/' + this.message_id;
        if (this.todo_id) return base + 'todos/' + this.todo_id;
        return base;
      }
    },
    started: {
      deps: ['id','finished'],
      cache: false,
      fn () {
        if (this.finished) return false;
        return !!this.id;
      }
    },
    type: {
      deps: ['project_id','message_id','todo_id'],
      cache: false,
      fn () {
        if (!this.message_id && !this.todo_id && !!this.project_id) return 'project';
        if (!!this.message_id) return 'message';
        if (!!this.todo_id) return 'todo';
      }
    },
    csvLine: {
      deps: ['csvLabel', 'day', 'startDisplay','endDisplay','totalHours','link', 'user.name'],
      fn () {
        return [this.csvLabel, this.day, this.startDisplay, this.endDisplay, this.totalHours, this.link].join(',');
      }
    }
  }

});