import Collection from 'ampersand-rest-collection';

export default Collection.extend({
  model: require('./comment')
});