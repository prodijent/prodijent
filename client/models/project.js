import app from 'ampersand-app'
import cacheMixin from 'ampersand-local-cache-mixin'
import Model from 'ampersand-model'
import ms from 'milliseconds'
import config from '../config';

module.exports = Model.extend(cacheMixin, {
  modelType: 'project',
  tts: ms.minutes(5),
  ttl: ms.weeks(1),
  storageKey () { return !!this.id ? 'project:' + this.id : undefined; },
  props: {
    id: 'number',
    last_event_at: 'date',
    updated_at: 'date',
    trashed: 'boolean',
    archived: 'boolean',
    color: 'string',
    starred: 'boolean',
    description: 'string',
    name: 'string',
    created_at: 'date',
    template: 'boolean',
    draft: 'boolean',
    is_client_project: 'boolean',
    accesses: 'object',
    attachments: 'object',
    calendar_events: 'object',
    documents: 'object',
    topics: 'object',
    todolists: 'object'
  },
  children: {
    "creator": require('./person')
  },
  initialize () {
    this.initStorage();
    this.on('change', this.writeToStorage, this);
  },
  url () {
    return this.parent.projectApiUrl;
  },
  ajaxConfig () {
    return {
      headers: {
        'Authorization': app.me.access_token ? 'Bearer ' + app.me.access_token : null,
        'User-Agent': 'Prodijent (https://www.prodijent.com)'
      }
    }
  }
});