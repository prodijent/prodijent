import Model from 'ampersand-model';

export default Model.extend({
  modelType: 'identity',
  session: {
    id: 'number',
    first_name: 'string',
    last_name: 'string',
    email_address: 'string'
  }
});
