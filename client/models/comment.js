import Model from 'ampersand-model';

export default Model.extend({
  modelType: 'comment',
  props: {
    id: 'number',
    content: 'string',
    created_at: 'date',
    updated_at: 'date',
    private: 'boolean',
    trashed: 'boolean'
  },
  children: {
    creator: require('./person')
  },
  collections: {
    attachments: require('./attachment-collection')
  }
});