import app from 'ampersand-app'
import Collection from 'ampersand-rest-collection'
import Project from './project'

export default Collection.extend({
  url () {
    if (app.me.currentAccount)
      return 'https://basecamp.com/' + app.me.currentAccount.id + '/api/v1/projects.json';
  },
  ajaxConfig () {
    return {
      headers: {
        'Authorization': app.me.access_token ? 'Bearer ' + app.me.access_token : null,
        'User-Agent': 'Prodijent (https://www.prodijent.com)'
      }
    }
  },
  model: Project
});