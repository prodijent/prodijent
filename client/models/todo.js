import app from 'ampersand-app'
import cacheMixin from 'ampersand-local-cache-mixin'
import Model from 'ampersand-model'
import ms from 'milliseconds'
import config from '../config';

module.exports = Model.extend(cacheMixin, {
  modelType: 'todo',
  tts: ms.minutes(5),
  ttl: ms.weeks(1),
  storageKey () { return 'todo:' + this.id; },
  props: {
    id: 'number',
    //todolist_id: 'number',
    content: 'string',
    //position: 'number',
    //due_at: 'date',
    //due_on: 'date',
    //created_at: 'date',
    //updated_at: 'date',
    //completed_at: 'boolean',
    //comments_count: 'number',
    //private: 'boolean',
    //trashed: 'boolean',
    //completed: 'boolean',
    //app_url: 'string'
  },
  children: {
    //creator: require('./person'),
    //assignee: require('./person')
  },
  initialize () {
    this.initStorage();
    this.on('change', this.writeToStorage, this);
  },
  url () {
    return this.parent.todoApiUrl;
  },
  ajaxConfig () {
    return {
      headers: {
        'Authorization': app.me.access_token ? 'Bearer ' + app.me.access_token : null,
        'User-Agent': 'Prodijent (https://www.prodijent.com)'
      }
    }
  }
});