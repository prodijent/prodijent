import Model from 'ampersand-model';

export default Model.extend({
  modelType: 'todolist',
  props: {
    id: 'number',
    key: 'string',
    name: 'string',
    byte_size: 'number',
    content_type: 'string',
    created_at: 'date',
    updated_at: 'date',
    url: 'string',
    app_url: 'string',
    thumbnail_url: 'string',
    private: 'boolean',
    trashed: 'boolean',
    tags: 'array',
    attachable: 'any'
  },
  children: {
    creator: require('./person')
  }
});