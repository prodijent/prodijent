import app from 'ampersand-app'
import Model from 'ampersand-model'
import config from '../config';

export default Model.extend({
  modelType: 'Share',
  urlRoot: config.host + '/api/v1/share',
  props: {
    id: 'number',
    user_id: 'number'
  },
  derived: {
    name: {
      deps: ['user_id'],
      cache: false,
      fn () {
        var person = app.people.find({ identity_id: this.user_id });
        if (person) return person.name;
      }
    },
    identity_id: {
      deps: ['user_id'],
      cache: false,
      fn () {
        var person = app.people.find({ identity_id: this.user_id });
        if (person) return person.identity_id;
      }
    }
  },
  ajaxConfig () { return { headers: { id: app.me.id } } }
});
