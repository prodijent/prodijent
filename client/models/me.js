import User from './user'
import cookie from '../lib/cookie'
import config from '../config'
import Share from './share'
import ShareCollection from './share-collection'
import ShareAccessCollection from './share-access-collection';
import afk from '../lib/afk/index'

module.exports = User.extend({
  modelType: 'Me',
  url: config.host + '/api/v1/user/me',
  session: {
    signedIn: { type: 'boolean', 'default': false },
    id: { type: 'string', 'default': require('../lib/id') },
    href: 'string',
    currentAccount: 'state'
  },
  initialize: function () {
    var me = this;
    setInterval(function () {
      afk.tick(function (time) {
        me.set({ idle: time });
      });
    }, 1000);
  },
  props: {
    idle: { type: 'number' }
  },
  collections: {
    shares: ShareCollection,
    shareAccesses: ShareAccessCollection
  },
  ajaxConfig () { return { headers: { id: this.id } } },
});
