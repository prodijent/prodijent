import app from 'ampersand-app'
import Collection from 'ampersand-rest-collection'
import Share from './share'
import config from '../config';

export default Collection.extend({
  url: config.host + '/api/v1/share',
  model: Share,
  ajaxConfig () { return { headers: { id: app.me.id } } },
});