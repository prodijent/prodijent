import Collection from 'ampersand-rest-collection'
import Model from './punch'
import config from '../config';

export default Collection.extend({
  url: config.host + '/api/v1/punch',
  model: Model,
  comparator(a, b) {
    return a.start < b.start ? +1 : -1;
  },
  ajaxConfig () { return { headers: { 'id': app.me.id } } }
});