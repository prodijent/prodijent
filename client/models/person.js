import Model from 'ampersand-model';

export default Model.extend({
  modelType: 'person',
  props: {
    id: 'number',
    identity_id: 'number',
    name: 'string',
    email_address: 'string',
    admin: 'boolean',
    trashed: 'boolean',
    avatar_url: 'string',
    fullsize_avatar_url: 'string',
    created_at: 'date',
    updated_at: 'date',
    url: 'string',
    app_url: 'string'
  },
  derived: {
    search: {
      deps: ['name','email_address'],
      fn () {
        return [this.name, this.email_address].join(' - ');
      }
    }
  }
});