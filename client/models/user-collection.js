import Collection from 'ampersand-rest-collection'
import User from './user'
import config from '../config';

export default Collection.extend({
  url: config.host + '/api/v1/user',
  model: User
});