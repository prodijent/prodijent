import app from 'ampersand-app'
import Collection from 'ampersand-rest-collection'
import ShareAccess from './share-access'
import config from '../config';

export default Collection.extend({
  model: ShareAccess
});