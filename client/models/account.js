import Model from 'ampersand-model'
import config from '../config';

export default Model.extend({
  modelType: 'account',
  props: {
    id: 'number',
    name: 'string',
    href: 'string',
    product: 'string'
  },
  derived: {
    show: {
      deps: ['product'],
      cache: false,
      fn () {
        return this.product === 'bcx';
      }
    }
  }
});