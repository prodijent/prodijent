import Model from 'ampersand-model';

export default Model.extend({
  modelType: 'bucket',
  props: {
    id: 'number',
    name: 'string',
    type: 'string',
    url: 'string',
    app_url: 'string'
  }
});