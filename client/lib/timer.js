import async from 'async'
import afk from 'afk'
import assign from 'lodash.assign'
import caps from 'lodash.capitalize'
import notify from '../lib/notify'
import Punch from '../models/punch'
import Account from '../models/account';

module.exports = Timer;

var Models = {
  project: require('../models/project'),
  message: require('../models/message'),
  todo: require('../models/todo')
};

function Timer(opts) {
  var timer = this;

  assign(timer, opts || {});

  timer.started = false;
  timer.punch = new Punch();

  var todo = timer.getId('todo');
  var message = timer.getId('message');
  var project = timer.getId('project');

  if (todo) {
    timer.punch.todo = new Models.todo({ id: todo });
    timer.type = 'todo';
  } else if (message) {
    timer.punch.message = new Models.message({ id: message });
    timer.type = 'message';
  } else {
    timer.type = 'project';
  }
  timer.punch.project = new Models.project({ id: project });
  timer.punch.account = new Account({ id: timer.getId('account') });

  if (timer.type !== 'project') {
    timer.punch.project.fetch();
  }

  timer.punch[timer.type].fetch({
    data: { pid: timer.punch.project.id },
    success: fetchSuccess,
    error(err) {
      if (err) throw err;
    }
  });

  function fetchSuccess() {
    if (timer.punch[timer.type].id) {
      //- Enable Start Button
      timer.el.querySelector('#prodijent-start').setAttribute('class', 'show');
      timer.removeListeners();
      timer.el.querySelector('#prodijent-start').addEventListener('click', timer.start.bind(timer));
      timer.el.querySelector('#prodijent-stop').addEventListener('click', timer.finishPunch.bind(timer));
      timer.el.querySelector('#prodijent-text').innerText = [caps(timer.type), '-', timer.punch[timer.type].get('label')].join(' ');
    } else {
      timer.el.querySelector('#prodijent-text').innerText = '';
      timer.el.querySelector('#prodijent-start').setAttribute('class','');
    }
  }

}

/*
 * Start the timer
 * */
Timer.prototype.start = function () {
  var timer = this;
  timer.punch.save();
  timer.punch.once('sync', function () {
    timer.started = true;
    timer.el.querySelector('#prodijent-start').setAttribute('class', '');
    timer.el.querySelector('#prodijent-stop').setAttribute('class', 'show');
    timer.updateTimeInterval = setInterval(timer.updateTime.bind(timer), 1000);
    timer.updatePunchInterval = setInterval(timer.updatePunch.bind(timer), 10000);
  });
};


/*
 * Update function to persist changes to server
 * */
Timer.prototype.update = function (args, cb) {
  var timer = this;
  args = args || null;
  if (!args) return;
  timer.punch.save(args, {
    patch: true,
    error: cb || null,
    success(model) {
      if (cb) cb(null, model);
    }
  })
};


/*
 * Updates time in the menu tray when tasks are active
 * */
Timer.prototype.updateTime = function () {
  var timer = this;
  if (!timer.punch[timer.type]) return;
  if (timer.punch.finished) return;
  afk.tick(function (time) {
    if (time > 60) {
      timer.finishPunch();
      notify.send('Time Stopped', 'The current timer was stopped due to inactivity.');
    }
  });
  timer.el.querySelector('#prodijent-timer').innerText = timer.punch.timeAgo;
};


/*
 * Sends put with updated end time to the server
 * */
Timer.prototype.updatePunch = function () {
  var timer = this;
  if (timer.punch.finished) return;
  timer.update({
    end: new Date()
  });
};


/*
 * Finishes task
 * */
Timer.prototype.finishPunch = function (e, exit) {
  var timer = this;
  clearInterval(timer.updateTimeInterval);
  clearInterval(timer.updatePunchInterval);
  timer.update({
    finished: true
  }, function (err) {
    if (err) return console.error(err);
    if (exit) process.exit(1);
    timer.reset();
  });
};

Timer.prototype.reset = function () {
  var timer = this;
  var opts = timer.punch.toJSON();
  timer.started = false;
  timer.punch = new Punch();
  timer.el.querySelector('#prodijent-start').setAttribute('class', 'show');
  timer.el.querySelector('#prodijent-stop').setAttribute('class', 'stop');
  timer.el.querySelector('#prodijent-timer').innerText = timer.punch.timeAgo;
  if (timer.type === 'todo')
    timer.punch.todo = new Models.todo({ id: opts.todo.id });
  else if (timer.type === 'message')
    timer.punch.message = new Models.message({ id: opts.message.id });
  timer.punch.project = new Models.project({ id: opts.project.id });
  timer.punch.account = new Account({})
};

Timer.prototype.removeListeners = function () {
  var timer = this;
  var oldEl, newEl;

  oldEl = timer.el.children['prodijent-start'];
  newEl = oldEl.cloneNode(true);
  oldEl.parentNode.replaceChild(newEl, oldEl);

  oldEl = timer.el.children['prodijent-stop'];
  newEl = oldEl.cloneNode(true);
  oldEl.parentNode.replaceChild(newEl, oldEl);
};

Timer.prototype.getId = function (type) {
  var timer = this;
  var path = timer.path.split('/')
    , index = path.indexOf(type + 's');
  if (type === 'account') return Number(path[1]);
  if (index === -1) return false;
  if (path.length < index + 1) return false;
  return Number(path[index + 1]);
};
