import Punch from '../models/punch'
import Project from '../models/project'
import Message from '../models/message'
import Todo from '../models/todo';

export default function (ids) {
  var punch = new Punch();

  if (!ids) return;

  punch.account = app.me.accounts.get(ids.account);
  if (!punch.account) return;

  punch.account_id = punch.account.id;
  punch.todo_id = ids.todo;
  punch.message_id = ids.message;
  punch.project_id = ids.project;

  if (!!punch.project_id) {
    punch.project.fetch();
  } else {
    punch.project = new Project();
  }

  if (!!punch.message_id || !!punch.todo_id) {
    punch[punch.type].fetch();
  } else {
    punch.message = new Message();
    punch.todo = new Todo();
  }
  return punch;
};