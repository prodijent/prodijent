import request from 'request'

export default function (punch) {

  if (!app.slack) return;

  let description = `${punch.project.name} - ${punch.message.subject || ''}${punch.todo.content || ''}`;

  let fields = [{
    title: 'Description',
    value: description,
    short: true
  }];


  fields.push({
    title: 'Start',
    value: punch.startDisplay,
    short: true
  });

  if (punch.finished)
    fields.push({
      title: 'End',
      value: punch.endDisplay,
      short: true
    });

  request({
    method: 'POST',
    uri: 'https://hooks.slack.com/services/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',
    json: true,
    body: {
      text: 'Prodijent Timer',
      icon_url: 'http://cl.ly/image/04213Y100b3C/Logo.png',
      username: 'Prodijent',
      attachments: [{
        color: '#87BB1A',
        pretext: `${app.me.identity.first_name} ${app.me.identity.last_name} has ${punch.finished ? 'finished' : 'started'} a timer.`,
        fallback: `${app.me.identity.first_name} ${app.me.identity.last_name} has ${punch.finished ? 'finished' : 'started'} a timer.`,
        fields: fields
      }]
    }
  }, (err, response, body) => {
    if (err) throw err;
    console.log(response, body);
  })
}