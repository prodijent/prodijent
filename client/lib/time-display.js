import moment from 'moment';

export default function (milliseconds) {
  var duration = moment.duration(milliseconds);
  var hours = Math.floor(duration.asHours());
  hours = hours < 10 ? '0' + hours : hours;
  return hours + moment.utc(milliseconds).format(":mm:ss");
};