import qs from './qs'
import fs from 'fs'
import touch from 'touch'

var idQs = qs('id');
var path = process.cwd() + '/.id';

touch.sync(path);

if (idQs)
  fs.writeFileSync(path, idQs, { encoding: 'utf8' });

module.exports = fs.readFileSync(path, { encoding: 'utf8' });
