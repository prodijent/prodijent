import app from 'ampersand-app'
import View from 'ampersand-view'
import assign from 'lodash.assign'
import each from 'lodash.foreach'
import Punch from '../models/punch'
import Project from '../models/project'
import Message from '../models/message'
import Todo from '../models/todo'
import addClass from 'amp-add-class'
import removeClass from 'amp-remove-class'
import config from '../config'
import setupNewPunch from '../lib/setup-new-punch'
import request from 'request'
import logo from '../lib/logo.svg'

export default View.extend({
  template: `
    <div>
      <header data-hook="header" class="drag">
        <div class="logo" data-hook="logo">
          <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 121.6 40" enable-background="new 0 0 121.6 27.3" xml:space="preserve">
          <filter id="dropshadow" height="130%">
            <feGaussianBlur in="SourceAlpha" stdDeviation="2"></feGaussianBlur> <!-- stdDeviation is how much to blur -->
            <feOffset dx="0" dy="0" result="offsetblur"></feOffset> <!-- how much to offset -->
            <feMerge>
              <feMergeNode></feMergeNode> <!-- this contains the offset blurred image -->
              <feMergeNode in="SourceGraphic"></feMergeNode> <!-- this contains the element that the filter is applied to -->
            </feMerge>
          </filter>
          <g>
            <path fill="#1E395B" d="M19.9,14l-0.4,0.1l-0.2,0.4c-0.5,1.2-1.8,2-3.1,2c-0.3,0-0.9-0.2-1.3-0.3l-0.3-0.1l-0.3,0.1
              c-0.4,0.2-1.8,0.7-3,0.7c-0.9,0-2.1-0.4-2.7-0.6l-4.5,1.7l0.6-4L4.2,14C2.9,13.2,2,11.8,2,10.3c0-1.5,0.8-2.9,2.1-3.6l0.3-0.2
              l0.1-0.4C5,4.5,6.5,3.4,8.2,3.4c0.3,0,0.5,0,0.8,0.1l0.4,0.1l0.3-0.3c0.7-0.6,1.6-0.9,2.6-0.9c0.9,0,1.8,0.3,2.6,0.9l0.3,0.3
              l0.4-0.1c0.2,0,0.5-0.1,0.7-0.1c1.7,0,3.1,1,3.7,2.6l0.1,0.3l0.3,0.2c1.4,0.7,2.2,2.1,2.2,3.7C22.7,11.8,21.6,13.4,19.9,14"></path>
            <path fill="#78B647" d="M80.7,3.5c0,1-0.8,1.8-1.8,1.8c-1,0-1.8-0.8-1.8-1.8c0-1,0.8-1.8,1.8-1.8C80,1.8,80.7,2.5,80.7,3.5"></path>
            <path fill="#78B647" d="M86.8,3.5c0,1-0.8,1.8-1.8,1.8c-1,0-1.8-0.8-1.8-1.8c0-1,0.8-1.8,1.8-1.8C86,1.8,86.8,2.5,86.8,3.5"></path>
            <path fill="#FFFFFF" d="M80.4,7.7l-2.6-1c-0.2-0.1-0.5,0.1-0.5,0.3v7.5c-0.5,1.1-1.4,2.6-2.4,2.6c-0.7,0-0.8-1.4-0.8-3.1V2.2
              c0-0.1-0.1-0.3-0.2-0.3l-2.6-1c-0.2-0.1-0.5,0.1-0.5,0.3v6.4c-0.6-1-1.6-1.7-3.1-1.7c-1.3,0-2.4,0.8-3.1,2l0,0
              c-0.8,1.4-1.6,2.4-2.5,3c-0.4-2.9-2.2-5-4.8-5c-3,0-4.9,2.9-4.9,6.5c0,0.9,0.1,1.8,0.4,2.7c-0.5,1-1.3,2-2.1,2
              c-1.8,0-1.1-5.4,0.6-7.9c0.1-0.1,0.1-0.3,0-0.4l-1.4-1.5c-0.1-0.1-0.2-0.1-0.4-0.1c-0.5,0.2-1.7,0.5-3,0c0,0,0.5-0.7,0.3-1.7
              c-0.1-0.8-1-1.5-2.1-1.4c-1.2,0.1-1.9,1.3-1.9,2.5c0,1,0.4,2,1.5,2.6c-1.2,4.2-4.1,5.5-5.1,5.8c-0.2,0.1-0.3,0.2-0.2,0.4l0.1,0.3
              c0,0.2,0.2,0.3,0.4,0.3c1.2-0.3,5.1-1.7,6.3-6.2c0.6,0.2,1.2,0.3,1.9,0.3c0,0-3.1,8.9,2.3,8.9c1.4,0,2.6-1.3,3.3-2.5
              c0.8,1.5,2.2,2.5,4,2.5c3,0,4.8-2.8,4.9-6.3c0.4-0.2,0.9-0.5,1.3-0.8c0,0.2,0,0.4,0,0.6c0,3.6,1.5,6.5,4.2,6.5
              c1.7,0,2.8-0.8,3.4-2.1c0.4,1.2,1.3,2.1,3.1,2.1c1.3,0,2.3-1.1,3.1-2.2v0.7c0,0.1,0.1,0.3,0.2,0.3l2.6,1c0.2,0.1,0.5-0.1,0.5-0.3V8
              C80.6,7.9,80.5,7.7,80.4,7.7 M59.2,11.4c-1.1-0.2-2-0.8-2.3-1.2c-0.9-1.1-0.8-2.2-0.3-2.6c0.2-0.1,0.4-0.2,0.7-0.2
              C58.8,7.5,59.2,9.2,59.2,11.4 M57.3,17.1c-1.5,0-1.9-2.2-1.9-4.8c0-0.6,0-1.1,0.1-1.6c0.1,0.2,0.2,0.3,0.3,0.5
              c0.5,0.7,1.8,1.6,3.5,1.9C59.2,15.3,58.7,17.1,57.3,17.1 M68.6,16.8c-1.2,0-2.1-2-2.1-4.5c0-2.5,0.9-4.5,2.1-4.5s2.2,1.3,2.2,4.5
              C70.8,15.5,69.8,16.8,68.6,16.8"></path>
            <path fill="#FFFFFF" d="M41.8,6.8c0-4.3-3.3-6.8-7.1-6.8c-4,0-6,3.1-6.6,4.1c-0.1,0.2,0,0.3,0.1,0.4l0.4,0.4
              C28.9,5,29.1,5,29.2,4.8C29.5,4.4,30,3.7,30.8,3v15.4c0,0.2,0.2,0.4,0.5,0.3l2.6-1c0.1-0.1,0.2-0.2,0.2-0.3v-4.4
              c0.4,0.3,1,0.5,1.8,0.6C38.9,14.1,41.8,11.1,41.8,6.8 M35.9,11.9c-1.5,0-1.8-1.3-1.8-1.6V3.1c0-0.2-0.1-0.3-0.3-0.3l-2-0.4
              c0.9-0.5,1.9-0.8,2.6-0.8c2.3,0,4,2,4,5.3C38.4,9.6,37.6,11.9,35.9,11.9"></path>
            <path fill="#FFFFFF" d="M121.4,14.1l-0.6-0.4c-0.2-0.1-0.4,0-0.5,0.2c-0.4,0.9-1.5,3.2-2.7,3.2c-0.7,0-0.8-1.4-0.8-3.1V6.4h2.1
              c0.2,0,0.3-0.1,0.3-0.2l0.3-0.9c0.1-0.2-0.1-0.4-0.3-0.4h-2.4V2.2c0-0.1-0.1-0.3-0.2-0.3l-2.6-1c-0.2-0.1-0.5,0.1-0.5,0.3v3.7H112
              c-0.2,0-0.3,0.1-0.3,0.2l-0.3,0.9c-0.1,0.2,0.1,0.5,0.3,0.5h1.8v7.3c-0.3,0.8-1.5,3.4-2.7,3.4c-0.7,0-0.8-1.4-0.8-3.1V9
              c0,0,0.1-3.1-2.7-3.1c-1.7,0-3.2,2.1-4.1,3.5v-2c0-0.1-0.1-0.3-0.2-0.3l-2.6-1C100.2,6,100,6.2,100,6.4v4.3
              c-0.4,1.1-2.7,6.7-5.6,6.7c-1.5,0-2.1-1.3-2.3-2.6c1.1-0.1,2.7-0.5,4.2-1.6c1.6-1.2,2.2-3.5,1.9-4.7c-0.3-1.4-1.6-2.6-3.8-2.6
              c-2.9,0-5.7,3-5.9,7c0,0.2,0,0.3,0,0.5c-0.5,0.7-1.1,1.6-2,2.5V8c0-0.1-0.1-0.3-0.2-0.3l-2.6-1c-0.2-0.1-0.5,0.1-0.5,0.3v11.5
              c-2.7,2-4.3,3.7-4.3,5.9c0,2,1.5,2.8,2.8,2.9c1.7,0.2,4.8-1,4.8-6.1v-3.5c1-0.8,1.8-1.6,2.4-2.4c0.7,2.1,2.7,3.7,5.2,3.7
              c2.9,0,4.9-3.3,5.9-5.3v3.8c0,0.1,0.1,0.3,0.2,0.3l2.6,1c0.2,0.1,0.5-0.1,0.5-0.3v-6.8c1-1.8,2-3.1,2.6-3.2c0.8,0,0.8,0.8,0.8,1.1
              c0,0.3,0,5.2,0,5.2c0,2.2,0.7,4.2,3.4,4.2c1.5,0,2.8-1.6,3.5-2.9c0.3,1.6,1.1,2.9,3.3,2.9c2.7,0,4.2-3.4,4.6-4.4
              C121.6,14.4,121.6,14.2,121.4,14.1 M81.6,26.1c-0.6,0-1.3-0.4-1.3-1.7c0-1.4,1-2.6,3-4.2v1C83.3,21.1,83.4,26.1,81.6,26.1 M92,12.4
              c0-0.5,0.5-4.8,2.7-4.8c1.2,0,1.5,1.1,1.2,2.3C95.7,11,94.5,13,92,13.4C92,13,92,12.6,92,12.4" style="filter:url(#dropshadow)"></path>
            <path fill="#FFFFFF" d="M21.8,4.8c-1-2.1-3.1-3.5-5.5-3.5c-0.2,0-0.4,0-0.6,0c-1-0.7-2.2-1-3.4-1c-1.2,0-2.4,0.3-3.4,1
              c-0.2,0-0.5,0-0.7,0c-2.4,0-4.6,1.5-5.4,3.8C1,6.3,0,8.2,0,10.3c0,2,1,3.9,2.7,5.1l-0.7,5.2c0,0.2,0.2,0.3,0.4,0.2l6.2-2.5
              c0.7,0.3,1.6,0.7,2.8,0.7c1.4,0,2.7-0.4,3.4-0.7c0.4,0.1,1,0.2,1.5,0.2c2,0,3.8-1.1,4.8-2.8c2.2-1,3.7-3.2,3.7-5.7
              C24.7,7.9,23.6,6,21.8,4.8 M19.9,14l-0.4,0.1l-0.2,0.4c-0.5,1.2-1.8,2-3.1,2c-0.3,0-0.9-0.2-1.3-0.3l-0.3-0.1l-0.3,0.1
              c-0.4,0.2-1.8,0.7-3,0.7c-0.9,0-2.1-0.4-2.7-0.6l-4.5,1.7l0.6-4L4.2,14C2.9,13.2,2,11.8,2,10.3c0-1.5,0.8-2.9,2.1-3.6l0.3-0.2
              l0.1-0.4C5,4.5,6.5,3.4,8.2,3.4c0.3,0,0.5,0,0.8,0.1l0.4,0.1l0.3-0.3c0.7-0.6,1.6-0.9,2.6-0.9c0.9,0,1.8,0.3,2.6,0.9l0.3,0.3
              l0.4-0.1c0.2,0,0.5-0.1,0.7-0.1c1.7,0,3.1,1,3.7,2.6l0.1,0.3l0.3,0.2c1.4,0.7,2.2,2.1,2.2,3.7C22.7,11.8,21.6,13.4,19.9,14"></path>
            <path fill="#78B647" d="M20.7,9.9c-0.8-0.5-1.6-0.9-2.5-1.1c0.7-2.4-0.8-3.8-0.8-3.8c-0.2-0.2-0.6-0.2-0.9,0
              c-0.2,0.2-0.2,0.6,0,0.9c0,0,0.9,0.9,0.5,2.6c-0.5-0.1-1-0.1-1.5-0.2c-0.7,0-1.3,0-2,0.1c-1.8-1.4-0.9-3.9-0.9-4
              c0.1-0.3,0-0.7-0.4-0.8s-0.7,0-0.8,0.4c-0.2,0.5-0.3,1.2-0.3,2c-0.7,0.3-2.3,0.8-3.6-0.4C7.3,5.5,7,5.5,6.7,5.8
              C6.5,6,6.5,6.4,6.8,6.6c0.9,0.8,1.9,1.1,2.8,1.1c0.7,0,1.3-0.1,1.8-0.3c0.1,0.5,0.4,1,0.8,1.5C10.7,9.3,9.4,10,8.3,11
              c-0.7-0.6-2.1-1.5-4-1.1C4,10,3.8,10.3,3.8,10.7c0.1,0.3,0.4,0.5,0.7,0.5c1.4-0.3,2.4,0.3,2.9,0.7c-0.6,0.7-1.2,1.5-1.6,2.4
              c-0.1,0.3,0,0.7,0.3,0.8c0.1,0,0.2,0.1,0.3,0.1c0.2,0,0.5-0.1,0.6-0.4c1.5-3.3,4.9-5.3,8.5-5.2c0,0,0,0,0.1,0
              c-0.1,0.4-0.4,1.3-0.3,2.3c-1.3-0.1-3.6,0.2-4.9,2.7c-0.2,0.3,0,0.7,0.3,0.8c0.1,0,0.2,0.1,0.3,0.1c0.2,0,0.4-0.1,0.5-0.3
              c1-2,3-2.1,4-2c0.2,0.6,0.5,1.3,1,1.8c0.1,0.1,0.3,0.2,0.5,0.2c0.1,0,0.3-0.1,0.4-0.2c0.3-0.2,0.3-0.6,0-0.9
              c-1.5-1.7-0.5-4.3-0.5-4.3c0,0,0,0,0,0C17.9,10,19,10.4,20,11c0.3,0.2,0.7,0.1,0.8-0.2C21,10.5,21,10.1,20.7,9.9"></path>
          </g>
          </svg>
        </div>
        <div class="nav">
          <div class="left">
            <div class="punch-label-container">
              <i class="fa-clock-o fa"></i>
              <span class="punch-label" data-hook="punch-label" class="select"></span>
            </div>
          </div>
          <div class="right nodrag">
            <i data-hook="close" class="btn fa fa-close close"></i>
            <i data-hook="minimize" class="btn fa fa-minus minimize"></i>
            <span data-hook="signed-in">
              <a data-hook="logout" class="btn logout">Log Out <i class="fa fa-sign-out"></i></a>
              <a data-hook="basecamp-toggle" class="btn basecamp-toggle">Basecamp</a>
              <a data-hook="shares" class="btn"><i class="fa fa-share-alt"></i></a>
              <a data-hook="time-log" class="btn"><i class="fa fa-clock-o"></i></a>
              <span data-hook="start-stop">
                <a data-hook="start" class="start btn">Start</a>
                <a data-hook="stop" class="stop btn">Stop <span data-hook="timer" class="timer"></span></a>
              </span>
            </span>
            <span data-hook="signed-out">
              <a data-hook="login" class="btn login">Sign In <i class="fa fa-sign-in"></i></a>
            </span>
            <i data-hook="loading" class="fa fa-refresh loading fa-spin"></i>
          </div>
        </div>
      </header>

      <iframe nwdisable nwfaketop data-hook="basecamp" class="basecamp" src="https://launchpad.37signals.com" frameborder="0"></iframe>
    </div>
  `,
  bindings: {
    'me.href': { type: 'attribute', name: 'href', hook: 'login' },
    'me.signedIn': { type: 'toggle', yes: '[data-hook=signed-in]', no: '[data-hook=signed-out]' },
    'model.label': { type: 'innerHTML', hook: 'punch-label' },
    'model.project.id': [
      { type: 'toggle', hook: 'start-stop' },
      { type: 'toggle', hook: 'project-container' },
      { type: 'booleanClass', yes: 'first', selector: '[data-hook=start], [data-hook=stop]' },
      { type: 'booleanClass', no: 'first', hook: 'basecamp-toggle' }
    ],
    'model.message.id': { type: 'toggle', hook: 'message-container' },
    'model.todo.id': { type: 'toggle', hook: 'todo-container' },
    'model.started': { type: 'toggle', no: '[data-hook=start]', yes: '[data-hook=stop]' },
    'loading': { type: 'booleanClass', name: 'show', hook: 'loading' },
    'timer': { type: 'text', hook: 'timer' },
    'showingBasecamp': [
      { type: 'toggle', hook: 'basecamp' },
      { type: 'booleanClass', name: 'active', hook: 'basecamp-toggle' },
      { type: 'booleanClass', name: 'hide', hook: 'header' }
    ],
    'showingShares': { type: 'booleanClass', name: 'active', hook: 'shares' },
    'showingTimeLog': { type: 'booleanClass', name: 'active', hook: 'time-log' }
  },
  events: {
    'click [data-hook=start]': 'startPunch',
    'click [data-hook=stop]': 'finishPunch',
    'click [data-hook=minimize]': 'minimizeWindow',
    'click [data-hook=close]': 'closeWindow',
    'click [data-hook=basecamp-toggle]': 'openBasecamp',
    'click [data-hook=shares]': 'openShares',
    'click [data-hook=time-log]': 'openTimeLog',
    'click [data-hook=logout]': 'logout'
  },
  props: {
    me: 'state',
    logo: 'string',
    currentPage: 'string',
    basecampPath: 'string',
    showingBasecamp: { type: 'boolean', default: true },
    showingShares: { type: 'boolean', default: false },
    showingTimeLog: { type: 'boolean', default: false },
    updatePunchInterval: 'number',
    timer: 'string',
    loading: 'boolean'
  },
  initialize () {
    var view = this;
    view.model = new Punch();
    view.logo = logo;
    setInterval(function () {
      try {
        var path = view.queryByHook('basecamp').contentDocument.location.pathname;
      } catch (err) { return }
      if (view.basecampPath !== path) view.basecampPath = path;
      view.timer = view.model ? view.model.total : '';
    }, 1000);

    view.on('change:basecampPath', view.setupNewPunches.bind(view));
    view.listenTo(app, 'basecamp-location', view.updateBasecampLocation);

  },
  openBasecamp () {
    this.showingBasecamp = true;
    this.showingShares = false;
    this.showingTimeLog = false;
  },
  openShares () {
    this.showingShares = true;
    this.showingBasecamp = false;
    this.showingTimeLog = false;
    app.nav('share');
  },
  openTimeLog () {
    this.showingTimeLog = true;
    this.showingBasecamp = false;
    this.showingShares = false;
    app.nav('punch');
  },
  setupNewPunches () {
    var view = this
      , ids = view.getPathIds();

    if (view.model.started && !view.model.finished) return;

    var punch = view.model = new Punch();

    punch.account = app.me.accounts.get(ids.account);
    if (!punch.account) return;

    punch.account_id = punch.account.id;
    punch.todo_id = ids.todo;
    punch.message_id = ids.message;
    punch.project_id = ids.project;

    if (!!punch.project_id) {
      punch.project.fetch();
    } else {
      punch.project = new Project();
    }

    if (!!punch.message_id || !!punch.todo_id) {
      punch[punch.type].fetch();
    } else {
      punch.message = new Message();
      punch.todo = new Todo();
    }
  },
  startPunch () {
    var view = this;
    view.model.save(null, {
      success (model) {
        new Notification('Timer Started', { body: 'Timer has begun'});
        view.updatePunchInterval = setInterval(view.updatePunch.bind(view), 10000);
        app.trigger('punch-update', model);
      },
      error(model, response, options) {
        new Notification('Communication Error', { body: 'Application is having trouble communicating with server.'});
      }
    });
  },
  updatePunch () {
    var view = this;
    console.log(app.me.idle);
    var timeout = 60 * 5; // 5 minutes
    if (app.me.idle > timeout) {
      new Notification('Inactivity: Timer Stopped');
      view.finishPunch();
    } else
      view.model.save({ finished: false }, {
        success (model) {
          app.trigger('punch-update', model)
        },
        error (model) {
          model.fetch({
            success (model) {
              if (model.finished){
                clearInterval(view.updatePunchInterval);
                new Notification('Timer Stopped', { body: model.project.label + ' - ' + model.content })
              }
            }
          });
          app.trigger('punch-update', model);
        }
      });
  },
  finishPunch () {
    var view = this;
    clearInterval(view.updatePunchInterval);
    view.model.save({ finished: true }, {
      success (model) {
        view.setupNewPunches();
        app.trigger('punch-update', model);
      }
    });
  },
  getPathIds () {
    var path = this.basecampPath.split('/');
    var ids = { account: Number(path[1]) };
    each(['project','todo','message'], function (type) {
      var index = path.indexOf(type + 's');
      if (index > -1 && path.length > index + 1)
        ids[type] = Number(path[index + 1]);
      else
        ids[type] = null;
    });
    return ids;
  },
  minimizeWindow () {
    app.gui.Window.get().minimize();
  },
  closeWindow () {
    app.gui.App.closeAllWindows();
  },
  updateBasecampLocation (url) {
    this.queryByHook('basecamp').src = url;
    this.openBasecamp()
  },
  logout () {
    request({
      method: 'GET',
      url: config.host + '/api/v1/user/logout',
      headers: { id: app.me.id }
    }, function () {
      window.location.assign('app://app/app.html');
    });
  }
});

