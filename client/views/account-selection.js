import app from 'ampersand-app'
import View from 'ampersand-view'
import AccountSelectionOption from './account-selection-option';

export default View.extend({
  template: '<div><span data-hook="selected-account"></span><ul class="account-list" data-hook="accounts"></ul></div>',
  bindings: {
    'model.name': { type: 'text', hook: 'selected-account' },
    'opened': { type: 'toggle', hook: 'accounts' }
  },
  props: {
    selected: 'any',
    name: 'string',
    opened: 'boolean'
  },
  events: {
    'click [data-hook=selected-account]': 'openAccounts'
  },
  render () {
    this.renderWithTemplate();
    this.renderCollection(app.me.accounts, AccountSelectionOption, this.queryByHook('accounts'));
    this.renderSubview(new AccountSelectionOption({ model: app.me.currentAccount }), this.queryByHook('accounts'));
    this.selectAccount(app.me.currentAccount);
  },
  selectAccount (model) {
    this.model = model;
    this.opened = false;
    app.me.currentAccount = model;
  },
  openAccounts () {
    this.opened = true;
  }
});
