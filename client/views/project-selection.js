import View from 'ampersand-view'
import find from 'lodash.find'

const ProjectSelectionOption = View.extend({
  template: '<li></li>',
  bindings: {
    'model.name': { type: 'text' }
  },
  events: {
    'click': 'selectItem'
  },
  selectItem () {
    this.parent.selectItem(this.model);
  }
});

export default View.extend({
  template: '<div><span data-hook="selected-project">Select Project</span><ul class="project-list" data-hook="projects"></ul></div>',
  bindings: {
    'name': { type: 'text', hook: 'selected-project' },
    'opened': { type: 'toggle', hook: 'projects' }
  },
  props: {
    selected: 'any',
    name: { type: 'string', default: 'All' },
    opened: 'boolean'
  },
  events: {
    'click [data-hook=selected-project]': 'openProjects'
  },
  initialize () {
    this.collection.fetch();
  },
  render () {
    this.renderWithTemplate();
    this.renderCollection(this.collection, ProjectSelectionOption, this.queryByHook('projects'));
  },
  selectItem (model) {
    this.model = model;
    this.name = model.name;
    this.opened = false;
  },
  openProjects () {
    this.opened = true;
  }
});