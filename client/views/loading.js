import View from 'ampersand-view'

export default View.extend({
  template: '<div><i class="fa fa-refresh loading fa-spin"></i></div>',
  autoRender: true
});

