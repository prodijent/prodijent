import View from 'ampersand-view'
import find from 'lodash.find'

const UserSelectionOption = View.extend({
  template: '<li></li>',
  bindings: {
    'model.name': { type: 'text' }
  },
  events: {
    'click': 'selectItem'
  },
  selectItem () {
    this.parent.selectItem(this.model);
  }
});

export default View.extend({
  template: '<div><span data-hook="selected-user">Me</span><ul class="user-list" data-hook="users"></ul></div>',
  bindings: {
    'model.name': { type: 'text', hook: 'selected-user' },
    'opened': { type: 'toggle', hook: 'users' }
  },
  props: {
    selected: 'any',
    name: 'string',
    opened: 'boolean'
  },
  events: {
    'click [data-hook=selected-user]': 'openUsers'
  },
  initialize () {
    var view = this;
    view.selectItem(view.collection.find({ user_id: app.me.identity.id }));
  },
  render () {
    var view = this;
    view.renderWithTemplate();
    view.renderCollection(view.collection, UserSelectionOption, view.queryByHook('users'));
  },
  selectItem (model) {
    this.model = model;
    this.opened = false;
  },
  openUsers () {
    this.opened = true;
  }
});