import View from 'ampersand-view'
import moment from 'moment'
import removeClass from 'amp-remove-class'
import addClass from 'amp-add-class';

export default View.extend({
  template: `
    <div class="punch-index-date-range">
      <label>Select Date Range</label>
      <ul data-hook="range-selector">
        <li data-hook="custom-range">Custom</li>
        <li data-hook="today" class="active">Today</li>
        <li data-hook="yesterday">Yesterday</li>
        <li data-hook="last-week">Last Week</li>
        <li data-hook="last-month">Last Month</li>
        <li data-hook="last-7-days">Last 7 Days</li>
        <li data-hook="last-30-days">Last 30 Days</li>
      </ul>
    </div>
  `,
  bindings: {
    open: { type: 'booleanClass', name: 'open', selector: 'li' }
  },
  events: {
    'mouseout [data-hook=range-selector]': 'onMouseOut',
    'click li': 'onClickRange'
  },
  props: {
    'open': 'boolean',
    'timeout': 'number'
  },
  onMouseOut () {
    var view = this;
    if (view.query('[data-hook=range-selector]:hover')) return;
    setTimeout(function () {
      if (view.query('[data-hook=range-selector]:hover')) return;
      view.open = false;
    }, 300);
  },
  resetClasses () {
    removeClass(this.queryByHook('custom-range'), 'active');
    removeClass(this.queryByHook('today'), 'active');
    removeClass(this.queryByHook('yesterday'), 'active');
    removeClass(this.queryByHook('last-week'), 'active');
    removeClass(this.queryByHook('last-month'), 'active');
    removeClass(this.queryByHook('last-7-days'), 'active');
    removeClass(this.queryByHook('last-30-days'), 'active');
  },
  setClass (hook) {
    this.resetClasses();
    addClass(this.queryByHook(hook), 'active');
  },
  onClickRange (e) {
    if (!this.open) {
      this.open = true;
      return;
    }
    this.open = false;
    var hook = e.target.getAttribute('data-hook');
    if (hook === 'today') {
      this.parent.startDate = moment().format('YYYY-MM-DD');
      this.parent.endDate = moment().format('YYYY-MM-DD');
    } else if (hook === 'yesterday') {
      this.parent.startDate = moment().subtract(1, 'days').format('YYYY-MM-DD');
      this.parent.endDate = moment().subtract(1, 'days').format('YYYY-MM-DD');
    } else if (hook === 'last-week') {
      this.parent.startDate = moment().subtract(1, 'weeks').startOf('isoWeek').format('YYYY-MM-DD');
      this.parent.endDate = moment().subtract(1, 'weeks').endOf('isoWeek').format('YYYY-MM-DD');
    } else if (hook === 'last-month') {
      this.parent.startDate = moment().subtract(1, 'months').startOf('month').format('YYYY-MM-DD');
      this.parent.endDate = moment().subtract(1, 'months').endOf('month').format('YYYY-MM-DD');
    } else if (hook === 'last-7-days') {
      this.parent.startDate = moment().subtract(7, 'days').format('YYYY-MM-DD');
      this.parent.endDate = moment().format('YYYY-MM-DD');
    } else if (hook === 'last-30-days') {
      this.parent.startDate = moment().subtract(30, 'days').format('YYYY-MM-DD');
      this.parent.endDate = moment().format('YYYY-MM-DD');
    }
    this.parent.fetch();
    this.setClass(hook);
  }

});

