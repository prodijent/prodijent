import app from 'ampersand-app'
import View from 'ampersand-view'
import ViewSwitcher from 'ampersand-view-switcher'
import NavView from './nav'
import Punch from '../models/punch'

export default View.extend({

  template: `
    <body>
      <div data-hook="nav" class="nav container"></div>
      <div data-hook="page-container" class="page-container"></div>
    </body>
  `,

  autoRender: true,

  initialize () {
    this.listenTo(app, 'page', this.setPage);
    this.listenTo(app, 'secure-page', this.setPageSecure)
  },

  events: {
    'click a[href]': 'handleLinkClick'
  },

  render () {
    this.renderWithTemplate({me: app.me});
    this.pageSwitcher = new ViewSwitcher(this.queryByHook('page-container'));
    this.renderSubview(new NavView({ me: app.me }), this.queryByHook('nav'));
  },

  setPageSecure (view) {
    if (!app.me.signedIn) return app.nav('login');
    this.pageSwitcher.set(view);
    app.currentView = view;
  },

  setPage (view) {
    this.pageSwitcher.set(view);
    app.currentView = view;
  },

  handleLinkClick (e) {
    var aTag = e.target;
    var local = aTag.host === window.location.host;
    if (local && !e.ctrlKey && !e.shiftKey && !e.altKey && !e.metaKey) {
      e.preventDefault();
      app.nav(aTag.pathname);
    }
  }


});
