import app from 'ampersand-app'
import View from 'ampersand-view'
import ShareCollection from '../models/share-collection'
import Collection from 'ampersand-collection'
import FormView from 'ampersand-form-view'
import InputView from 'ampersand-input-view'
import Person from '../models/person'
import SearchView from './search'
import fs from 'fs'

var PersonView = View.extend({
  template: `
    <div class="item">
      <span data-hook="name"></span>
      <span data-hook="remove"><i class="fa fa-remove"></i></span>
    </div>
  `,
  bindings: {
    'model.name': { type: 'text', hook: 'name' }
  },
  events: {
    'click': 'removePerson'
  },
  removePerson () {
    app.me.shares.find({ share_id: this.model.identity_id }).destroy();
    this.model.destroy();
  }
});

export default View.extend({
  template: `
    <div class="share-page">
      <h3>Allow another Basecamp user to review your time log.</h3>
      <div class="share-search" data-hook="search"></div>
      <div class="current-shares">
        <div class="label">Current users with access to see your time.</div>
        <div class="items" data-hook="current-shares"></div>
      </div>
      <h3>Slack Integration</h3>
      <form>
        <div data-hook="webhook"></div>
        <button type="submit">Save</button>
      </form>
    </div>
  `,
  render () {
    var view = this;

    view.renderWithTemplate();

    view.model = new Person();

    view.search = new SearchView({ collection: app.people });

    view.renderSubview(view.search, view.queryByHook('search'));

    view.people = new Collection(app.people.filter(function (person) { return app.me.shares.find({ share_id: person.identity_id }); }));
    view.renderCollection(view.people, PersonView, view.queryByHook('current-shares'), {
      filter (model) {
        return model.identity_id !== app.me.identity.id
      }
    });

    view.search.on('change:model', function (searchView) {
      app.me.shares.create({ share_id: searchView.model.identity_id }, {
        success (model) {
          view.people.add(app.people.find({ identity_id: model.share_id }))
        }
      });
    });

    view.slackIntegration = new FormView({
      autoRender: true,
      autoAppend: false,
      el: this.query('form'),
      fields: [
        new InputView({
          name: 'webhook',
          label: 'Webhook',
          placeholder: 'https://hooks.slack.com/services/xxxx/xxxx/xxxx',
          el: this.queryByHook('webhook')
        })
      ],
      submitCallback (obj) {
        app.slack = obj.webhook;
        fs.writeFileSync(`${process.cwd()}/.slack`, obj.webhook);
      }
    });


  }
});