import View from 'ampersand-view'

export default View.extend({
  template: '<li></li>',
  bindings: {
    'model.name': { type: 'text' },
    'model.show': { type: 'toggle' }
  },
  events: {
    'click': 'selectAccount'
  },
  selectAccount () {
    this.parent.selectAccount(this.model);
  }
});