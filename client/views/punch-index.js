import View from 'ampersand-view'
import ViewSwitcher from 'ampersand-view-switcher'
import assign from 'lodash.assign'
import find from 'lodash.find'
import fs from 'fs'
import moment from 'moment'
import Item from './punch-item'
import timeDisplay from '../lib/time-display'
import PersonCollection from '../models/person-collection'
import ProjectCollection from '../models/project-collection'
import ProjectSelection from './project-selection'
import PunchIndexChartItem from './punch-index-chart'
import UserSelection from './user-selection'
import AccountSelection from './account-selection'
import DateRange from './punch-index-date-range'
import config from '../config'
import _get from 'lodash.get'


var ChartDay = View.extend({
  template: '<td class="chart-day"></td>',
  props: { day: 'date' },
  render () {
    var view = this;
    view.renderWithTemplate();
    view.renderCollection(view.collection, PunchIndexChartItem, view.el, {
      filter (model) {
        return model.start.toLocaleDateString() === view.day.toLocaleDateString();
      }
    });
  }
});

var ChartContainer = View.extend({
  template: '<tr class="chart-container"></tr>',
  props: {
    start: 'string',
    end: 'string'
  },
  render () {
    var view = this;
    view.renderWithTemplate();
    var start = moment(view.start);
    var length = moment(view.end).diff(view.start, 'days');
    for (var x = 0; x <= length; x++) {
      view.renderSubview(new ChartDay({ collection: view.collection, day: moment(start).add(x, 'day') }), view.el);
    }
  }
});


export default View.extend({
  template: `
    <div>
      <div class="time-log" data-hook="time-log">
        <form class="filters" data-hook="search-container">
          <div class="start">
            <label for="start">Start</label>
            <input type="date" id="start" required class="form-input" data-hook="start-range-date"/>
          </div>
          <div class="end">
            <label for="end">End</label>
            <input type="date" id="end" required class="form-input" data-hook="end-range-date"/>
          </div>
          <div data-hook="date-range" class="date-range"></div>
          <div class="user">
            <label>Select User</label>
            <div class="users" data-hook="select-user-view"></div>
          </div>
          <div class="project">
            <label>Select Project</label>
            <div class="projects" data-hook="select-project-view"></div>
          </div>
          <div class="refresh"><i data-hook="refresh" class="fa fa-refresh"></i><i data-hook="spinner" class="fa fa-spinner fa-spin"></i></div>
        </form>
        <table class="chart" data-hook="chart"></table>
        <div class="chart-text" data-hook="chart-text"></div>
        <table class="punch-items">
          <thead>
          <tr>
            <th class="current"></th>
            <th>Id</th>
            <th>Task</th>
            <th>Date</th>
            <th>Start</th>
            <th>End</th>
            <th>Total</th>
          </tr>
          </thead>
          <tbody data-hook="collection"></tbody>
          <tfoot>
            <tr>
              <td colspan="5">
                <ul class="legend">
                  <li class="current">Active</li>
                  <li class="edited">Edited</li>
                </ul>
                <a data-hook="csv" class="download">Download CSV</a>
                <input data-hook="csv-input" type="file" nwsaveas="export.csv" accept=".csv" style="display:none" nwworkingdir=""/>
              </td>
              <td>Total Time</td>
              <td data-hook="total-time"></td>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  `,

  bindings: {
    'time': { type: 'text', hook: 'total-time' },
    'homeDir': { type: 'attribute', name: 'nwworkingdir', hook: 'csv-input' },
    'defaultCsvPath': { type: 'attribute', name: 'nwsaveas', hook: 'csv-input' },
    'fetching': { type: 'toggle', yes: '[data-hook=spinner]', no: '[data-hook=refresh]' },
    'startDate': { type: 'attribute', name: 'value', hook: 'start-range-date' },
    'endDate': { type: 'attribute', name: 'value', hook: 'end-range-date' }
  },

  props: {
    chart: 'any',
    path: 'string',
    time: 'string',
    user: 'state',
    project: 'state',
    editing: 'boolean',
    csvContent: 'string',
    defaultCsvPath: 'string',
    fetching: 'boolean',
    startDate: { type: 'string', default() { return moment().format('YYYY-MM-DD'); } },
    endDate: { type: 'string', default() { return moment().format('YYYY-MM-DD'); } }
  },

  derived: {
    chartData: {
      cache: false,
      fn () {
        return this.collection.models.map(function (model) {
          return model.chartData;
        });
      }
    },
    homeDir: {
      fn () {
        return process.env[(process.platform == 'win32') ? 'USERPROFILE' : 'HOME'];
      }
    }
  },

  events: {
    'change [data-hook=start-range-date]': 'onChangeStart',
    'change [data-hook=end-range-date]': 'onChangeEnd',
    'keydown [data-hook=start-range-date], [data-hook=end-range-date]': 'onPreventKeyUp',
    'click [data-hook=csv]': 'exportCsv',
    'change [data-hook=csv-input]': 'saveCsv',
    'click [data-hook=refresh]': 'fetch'
  },

  onPreventKeyUp (e) {
    e.preventDefault();
  },

  onChangeStart (e) {
    this.dateRange.setClass('custom-range');
    this.startDate = e.target.value;
    var date = moment(e.target.value);
    e.target.setAttribute('max', date.add(1, 'month').format('YYYY-MM-DD'));
    e.target.setAttribute('min', date.subtract(2, 'month').format('YYYY-MM-DD'));
    this.fetch();
  },

  onChangeEnd (e) {
    this.dateRange.setClass('custom-range');
    this.endDate = e.target.value;
    var date = moment(e.target.value);
    e.target.setAttribute('max', date.add(1, 'month').format('YYYY-MM-DD'));
    e.target.setAttribute('min', date.subtract(2, 'month').format('YYYY-MM-DD'));
    this.fetch();
  },

  initialize () {
    var view = app.temp = this;
    view.user = app.me;
    view.listenTo(app, 'punch-update', view.updatePunch);
    view.collection.on('change:milliseconds add', view.totalTime.bind(view));
    app.punches = view.collection;
  },

  render () {
    var view = this;
    view.renderWithTemplate();
    view.dateRange = view.renderSubview(new DateRange(), view.queryByHook('date-range'));
    var date = moment(view.queryByHook('start-range-date').value);
    view.queryByHook('start-range-date').setAttribute('max', date.add(1, 'month').format('YYYY-MM-DD'));
    view.queryByHook('start-range-date').setAttribute('min', date.subtract(2, 'month').format('YYYY-MM-DD'));
    view.queryByHook('end-range-date').setAttribute('max', date.add(1, 'month').format('YYYY-MM-DD'));
    view.queryByHook('end-range-date').setAttribute('min', date.subtract(2, 'month').format('YYYY-MM-DD'));

    view.renderCollection(view.collection, Item, view.queryByHook('collection'));

    view.chart = new ViewSwitcher(view.queryByHook('chart'));

    view.selectedUser = view.renderSubview(new UserSelection({ collection: app.me.shareAccesses }), view.queryByHook('select-user-view'));
    view.selectedUser.on('change:model', view.fetch.bind(view));

    view.selectedProject = view.renderSubview(new ProjectSelection({ collection: new ProjectCollection() }), view.queryByHook('select-project-view'));
    view.selectedProject.on('change:model', view.fetch.bind(view));

    if (app.me.signedIn)
      view.fetch();
    else
      app.me.once('sync', view.fetch.bind(view));
  },


  fetch () {
    var view = this;
    if (view.editing) return;
    view.fetching = true;

    view.defaultCsvPath = view.startDate.replace(/\W+/g, "") + '-' + view.endDate.replace(/\W+/g, "") + '.csv';

    view.chart.set(new ChartContainer({ collection: view.collection, start: view.startDate, end: view.endDate }));

    view.collection.fetch({
      timeout: 20000,
      data: {
        reset: true,
        start: view.startDate,
        end: view.endDate,
        user_id: view.selectedUser.model.identity_id,
        project_id: _get(view, 'selectedProject.model.id', null)
      },
      success (collection) {
        view.fetching = false;
        collection.each(function (model) {
          model.project.fetch();
          if (model.type === 'message')
            model.message.fetch();
          if (model.type === 'todo')
            model.todo.fetch();
        })
      },
      error () {
        view.fetching = false;
      }
    });
  },

  totalTime () {
    if (!this.collection.length) return;
    var milliseconds = this.collection.pluck('milliseconds').reduce(function (a, b) {
      return a + b;
    });
    var seconds = milliseconds / 1000;
    var minutes = seconds / 60;
    var hours = minutes / 60;
    this.time = hours.toFixed(2);
  },

  updatePunch (model) {
    this.collection.add(model, { merge: true });
  },

  exportCsv () {
    var view = this;
    view.csvContent = 'User, Task, Day, Start, End, Total (hours), Link\n';
    view.csvContent += view.collection.map(function (item) {
      return view.selectedUser.model.name.replace(/[^\w\s]/gi, '') + ',' + item.csvLine;
    }).join('\n');
    view.queryByHook('csv-input').click();
  },

  saveCsv (e) {
    var view = this;
    fs.writeFile(e.target.value, view.csvContent, function(err) {
      if(err) alert("error"+err);
    });
  },

});


