import View from 'ampersand-view'

export default View.extend({
  template: '<span></span>',
  bindings: {
    'model.chartStyles': { type: 'attribute', name: 'style' }
  },
  events: {
    'mouseenter': 'overEvent',
    'mouseleave': 'outEvent'
  },
  overEvent () {
    this.model.over = true;
  },
  outEvent () {
    this.model.over = false;
  }
});