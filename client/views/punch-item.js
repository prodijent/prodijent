import app from 'ampersand-app'
import View from 'ampersand-view'
import moment from 'moment'

export default View.extend({
  template: `
    <tr>
      <td class="indicators-tools">
        <span class="indicators" data-hook="indicators">
          <i class="current" data-hook="current"></i>
          <i class="edited" data-hook="edited"></i>
        </span>
        <span class="tools" data-hook="tools">
          <i class="fa fa-floppy-o save" data-hook="save"></i>
          <i class="fa fa-pencil" data-hook="edit"></i>
          <i class="fa fa-trash-o" data-hook="delete"></i>
        </span>
      </td>
      <td data-hook="id"></td>
      <td class="task">
        <span class="content" data-hook="content"></span>
        <i class="fa fa-external-link-square" data-hook="link"></i>
      </td>
      <td class="day">
        <input type="date" required data-hook="day" />
      </td>
      <td>
        <input type="time" required data-hook="start" />
      </td>
      <td>
        <input type="time" required data-hook="end" />
      </td>
      <td data-hook="total"></td>
    </tr>
  `,
  bindings: {
    'model.id': { type: 'text', hook: 'id' },
    'model.label': { type: 'innerHTML', hook: 'content' },
    'model.day': { type: 'attribute', name: 'value', hook: 'day' },
    'model.dayClass': { type: 'class', hook: 'day' },
    'model.startElementValue': { type: 'attribute', name: 'value', selector: '[data-hook~=start]' },
    'model.endElementValue': { type: 'attribute', name: 'value', selector: '[data-hook~=end]' },
    'model.totalHours': { type: 'text', hook: 'total' },
    'model.finished': { type: 'toggle', hook: 'finished' },
    'model.started': { type: 'toggle', hook: 'current' },
    'model.edited': { type: 'toggle', hook: 'edited' },
    'model.over': { type: 'booleanClass' },
    'editing': [
      { type: 'booleanAttribute', no: 'disabled', hook: 'start' },
      { type: 'booleanAttribute', no: 'disabled', hook: 'end' },
      { type: 'booleanAttribute', no: 'disabled', hook: 'day' },
      { type: 'toggle', no: '[data-hook=edit],[data-hook=indicators]', yes: '[data-hook=save], [data-hook=delete]' },
      { type: 'booleanClass', name: 'editing' }
    ]
  },
  events: {
    'mouseenter': 'onMouseOver',
    'mouseleave': 'onMouseLeave',
    'change [data-hook=start]': 'startTimeChange',
    'change [data-hook=end]': 'endTimeChange',
    'change [data-hook=day]': 'dateChange',
    'click [data-hook=edit]': 'editTime',
    'click [data-hook=save]': 'updateTime',
    'click [data-hook=link]': 'onClickLink',
    'click [data-hook=delete]': 'onClickDelete'
  },
  props: {
    editing: { type: 'boolean', default: false }
  },
  onMouseOver () {
    this.model.over = true;
  },
  onMouseLeave () {
    this.model.over = false;
  },
  onClickLink () {
    app.trigger('basecamp-location', this.model.link);
  },
  onClickDelete () {
    if(confirm("Are you sure?"))
      this.model.destroy();
  },
  editTime (e) {
    if (this.editing) return;
    this.editing = true;
    this.parent.editing = true;
    e.target.focus();
  },
  updateTime () {
    this.editing = false;
    this.parent.editing = false;
    this.model.save({ edited: true, start: this.model.start, end: this.model.end }, { patch: true });
  },
  startTimeChange (e) {
    var time = e.target.value.split(':');
    this.model.start = moment(this.model.start).set('hour', time[0]).set('minute',time[1]);
  },
  endTimeChange (e) {
    var time = e.target.value.split(':');
    this.model.end = moment(this.model.end).set('hour', time[0]).set('minute',time[1]);
  },
  dateChange (e) {
    var start = moment(this.model.start);
    var end = moment(this.model.end);
    this.model.start = moment(e.target.value).set('hour', start.format('HH')).set('minute', start.format('mm')).toDate();
    this.model.end = moment(e.target.value).set('hour', end.format('HH')).set('minute', end.format('mm')).toDate();
  }
});



