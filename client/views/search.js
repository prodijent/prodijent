import View from 'ampersand-view'
import each from 'lodash.foreach'
import find from 'lodash.findwhere'
import startsWith from 'lodash.startswith'

var ItemView = View.extend({
  template: '<li data-hook="person-option"></li>',
  bindings: {
    active: { type: 'booleanClass', name: 'active' },
    show: { type: 'toggle', hook: 'person-option' },
    'model.search': { type: 'text' }
  },
  props: {
    active: { type: 'boolean', default: false },
    show: { type: 'boolean' }
  },
  events: {
    click: 'select'
  },
  render () {
    var view = this;
    view.renderWithTemplate();
    view.parent.on('change:currentText', function (parent) {
      view.show = view.model.search.indexOf(parent.currentText) !== -1
    })
  },
  select (e) {
    this.parent.select(this.model);
  }
});


export default View.extend({
  template: `
    <div class='form-element'>
      <label for='search-input' data-hook="label">Search</label>
      <input type='text' data-hook="input" autocomplete="off" id='search-input'>
      <ul class="options" data-hook="options"></ul>
    </div>
  `,

  render () {
    var view = this;
    view.renderWithTemplate();
    view.items = view.renderCollection(view.collection, ItemView, view.queryByHook('options'));
  },

  events: {
    'keyup input': 'keyup',
    'keydown input': 'keydown',
    'blur input': 'blur'
  },

  bindings: {
    show: { type: 'toggle', hook: 'options' },
    currentText: { type: 'attribute', name: 'value', hook: 'input' }
  },

  props: {
    currentText: { type: 'string' },
    currentIndex: { type: 'number', default: 0 },
    show: { type: 'boolean' },
  },

  keydown (e) {
    var view = this;
    if (e.keyCode == 38) return view.move(-1);
    if (e.keyCode == 40) return view.move(+1);
    if (e.keyCode == 13) return view.onEnter(e);
    if (e.keyCode == 27) return view.show = false;
  },

  keyup (e) {
    var view = this;
    var keyword = e.target.value;
    if (view.isChanged(keyword)) {
      if (view.isValid(keyword))
        view.filter(keyword);
      else
        view.show = false;
      view.currentText = keyword;
    }
  },

  blur () {
    var view = this;
    setTimeout(function () {
      view.show = false;
    }, 300)
  },

  move (position) {
    var view = this;
    var max = view.collection.length;
    if (view.currentIndex === 0 && position === -1)
      return;
    if (view.currentIndex === max && position === 1)
      return;
    view.currentIndex += position;
    each(view.items.views, function(subview, index) {
      subview.active = view.currentIndex === (index + 1);
    })
  },

  select (model) {
    var view = this;
    view.show = false;
    view.model = model;
    view.currentText = view.model.search;
    view.queryByHook('input').value = '';
  },

  onEnter (e) {
    e.preventDefault();
    var view = this;
    var subview = find(view.items.views, { active: true });
    view.select(subview.model);
    return false;
  },

  isChanged (keyword) {
    return this.currentText != keyword;
  },

  filter (keyword) {
    this.show = true;
  },

  isValid (keyword) {
    return keyword.length > 0
  }

});