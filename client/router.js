import app from 'ampersand-app'
import Router from 'ampersand-router'
import ShareView from './views/share'
import PunchIndexView from './views/punch-index'
import PunchCollection from './models/punch-collection'

let punches = new PunchCollection();

module.exports = Router.extend({

  routes: {
    'share': 'share',
    'punch': 'punch',
    '(*path)': 'catchAll'
  },


  share () {
    app.trigger('page', new ShareView({ model: app.me }));
  },

  punch () {
    app.trigger('page', new PunchIndexView({ model: app.me, collection: punches }));
  },

  catchAll () {
    this.redirectTo('/')
  }

});


