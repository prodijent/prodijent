import app from 'ampersand-app'
import gui from 'nw.gui'
import domReady from 'domready'
import bind from 'lodash.bind'
import Me from './models/me'
import Router from './router'
import config from './config'
import PeopleCollection from './models/person-collection'
import LoadingView from './views/loading'
import ShareAccess from './models/share-access'
import MainView from './views/main'
import css from './sass/main.css'
import fs from 'fs'

app.extend({
  gui: gui,
  me: new Me(),
  people: new PeopleCollection(),
  router: new Router(),
  fetch (cb) {
    let slackFilePath = `${process.cwd()}/.slack`;
    if (fs.existsSync(slackFilePath))
      app.slack = fs.readFileSync(slackFilePath, { encoding: 'utf8' });
    app.me.fetch({
      error: cb,
      success (me) {
        if (me.accounts.length)
          me.signedIn = true;
        else
          return cb();
        me.currentAccount = me.accounts.find({product: 'bcx'});
        app.people.fetch({
          error: cb,
          success () {
            me.shares.fetch({
              error: cb,
              success() {
                me.shareAccesses.add(new ShareAccess({ user_id: me.identity.id }));
                cb();
              }
            });
          }
        });
      }
    });
  },
  init () {
    app.loader();
    app.fetch(() => {
      new MainView({el: document.body});
      app.router.history.start({ pushState: false });
    })
  },
  loader () {
    var loader = document.createElement("div");
    loader.setAttribute('class', 'loader');
    loader.appendChild(document.createElement('i'))
    document.body.appendChild(loader);
  },
  nav (page) {
    var url = (page.charAt(0) === '/') ? page.slice(1) : page;
    this.router.history.navigate(url, { trigger: true });
  }
});

if (config.isDev)
  gui.App.registerGlobalHotKey(new gui.Shortcut({
    key : "Ctrl+Alt+I",
    active  () {
      gui.Window.get().showDevTools();
    }
  }));

domReady(app.init);
window.app = app;

