var gui = require('nw.gui');
var fs = require('fs');

gui.App.addOriginAccessWhitelistEntry('http://localhost:3000', 'app', 'app', true);
gui.App.addOriginAccessWhitelistEntry('https://www.prodijent.com', 'app', 'app', true);
gui.App.addOriginAccessWhitelistEntry('https://launchpad.37signals.com', 'app', 'app', true);
gui.App.addOriginAccessWhitelistEntry('https://basecamp.com', 'app', 'app', true);

if (process.platform === "darwin") {
  var menu = new gui.Menu({type: "menubar"});
  menu.createMacBuiltin && menu.createMacBuiltin('Prodijent');
  gui.Window.get().menu = menu;
}

var win = gui.Window.open('app.html', {
  title: 'Prodijent',
  toolbar: process.env.isDev || false,
  frame: process.env.isDev || false,
  width: 1060,
  height: 600
});

win.on('close', process.exit);
process.on('exit', process.exit);
process.on('SIGINT', process.exit);
process.on('SIGTERM', process.exit);
