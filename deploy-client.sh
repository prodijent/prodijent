#!/usr/bin/env bash

TIME=`date +%s`
FILENAME="app.build.$TIME.js"
webpack
/usr/local/bin/aws s3 cp "client/app.build.js" "s3://prodijent/$FILENAME" --acl=public-read --profile=threecoasts
/Users/brianberlin/.rvm/gems/ruby-2.0.0-p451/bin/heroku config:set build="$TIME"